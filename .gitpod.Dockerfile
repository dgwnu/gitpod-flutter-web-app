FROM gitpod/workspace-base

# Initialize apt-get for additional packages
RUN sudo apt update -y
RUN sudo apt upgrade -y

USER gitpod
# Linux Flutter Install
# Based on: https://docs.flutter.dev/get-started/install/linux#install-flutter-manually
RUN mkdir downloads
RUN curl https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.7.0-stable.tar.xz --output downloads/flutter.tar.xz
RUN tar xf downloads/flutter.tar.xz
RUN echo 'export PATH="$PATH:~/flutter/bin"' >> ~/.bashrc
